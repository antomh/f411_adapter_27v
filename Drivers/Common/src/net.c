/*
 *  w5500_general.c
 *
 *  Created on: Jan 19, 2021
 *  Author: Anton Shein<anton-shein2008@yandex.ru>
 *  ----------------------------------------------------------------------
 *  Copyright (C) Anton Shein, 2021
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later Bversion.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ----------------------------------------------------------------------
 */

#include "main.h"
#include "socket.h"
#include "wizchip_conf.h"
#include "w5500.h"
#include "net.h"
#include "usart.h"
#include "string.h"


/* Init variables */
wiz_NetInfo g_WIZNETINFO;
uint8_t         mac[6] =        {0x40, 0x08, 0xDC, 0x00, 0xAB, 0xCD},
                ip[4] =         {192, 168, 11, 155},
                netmask[4] =    {255, 255, 255, 0},
                gateway[4] =    {192, 168, 1, 1},
                dnsserv[4] =    {0, 0, 0, 0};
extern SPI_HandleTypeDef        W5500_USING_SPI;
extern UART_HandleTypeDef       huart1;


/**
 * @brief  Select W5500 at SPI transaction
 */
void W5500_select(void)
{
    HAL_GPIO_WritePin(W5500_GPIO_PORT, W5500_GPIO_SCS_PIN, GPIO_PIN_RESET);
}


/**
 * @brief  Deselect W5500 after SPI transaction
 */
void W5500_deselect(void)
{
    HAL_GPIO_WritePin(W5500_GPIO_PORT, W5500_GPIO_SCS_PIN, GPIO_PIN_SET);
}


/**
 * @brief  Reset W5500 in case of chip freeze
 */
void W5500_reset(void)
{
    HAL_GPIO_WritePin(W5500_GPIO_PORT, W5500_GPIO_RST_PIN, GPIO_PIN_RESET);
    HAL_Delay(100);
    HAL_GPIO_WritePin(W5500_GPIO_PORT, W5500_GPIO_RST_PIN, GPIO_PIN_SET);
    HAL_Delay(100);
}


/**
 * @brief  Read W5500 buffer
 */
void W5500_ReadBuff(uint8_t* buff, uint16_t len)
{
    HAL_SPI_Receive(&W5500_USING_SPI, buff, len, HAL_MAX_DELAY);
}


/**
 * @brief  Write W5500 buffer
 */
void W5500_WriteBuff(uint8_t* buff, uint16_t len)
{
    HAL_SPI_Transmit(&W5500_USING_SPI, buff, len, HAL_MAX_DELAY);
}


/**
 * @brief  Write byte to W5500
 */
void W5500_WriteByte(uint8_t byte)
{
    W5500_WriteBuff( &byte, sizeof(byte) );
}


/**
 * @brief  Read byte from W5500
 */
uint8_t W5500_ReadByte(void)
{
    uint8_t byte;
    W5500_ReadBuff( &byte, sizeof(byte) );
    return byte;
}


/*
 * @brief  W5500 initialazation, set IP, MAC, NETMASK, GATEAWAY, DNS
 * @retval Initialazation result - success or not
 */
uint8_t net_init(void)
{
    /* Send info to UART console */
//    {
//        char sbuf[] = {"Initialisation starts..."};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }

    /* reset w5500 chip */
    W5500_reset();

    /* Send info to UART console */
//    {
//        char sbuf[] = {"Register W5500 callbacks"};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }

    /* register callbacks to send and receive data from w5500 */
    reg_wizchip_cs_cbfunc( W5500_select, W5500_deselect );
    reg_wizchip_spi_cbfunc( W5500_ReadByte , W5500_WriteByte );
    reg_wizchip_spiburst_cbfunc( W5500_ReadBuff, W5500_WriteBuff );

    /* Send info to UART console */
//    {
//        char sbuf[] = {"Init wizchip"};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }

    uint8_t W5500_fifo_size[2][8] = { {2, 2, 2, 2, 2, 2, 2, 2},
                                      {2, 2, 2, 2, 2, 2, 2, 2} };
    if ( ctlwizchip( CW_INIT_WIZCHIP , (void*)W5500_fifo_size ) == -1 )
    {
        return W5500_ERR;
    }

    /* Send info to UART console */
//    {
//        char sbuf[] = {"Sending MAC, IP, etc..."};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }

    for ( uint8_t i = 0; i <= 5; ++i )
    {
        g_WIZNETINFO.mac[i] = mac[i];
    }
    for ( uint8_t i = 0; i <= 3; ++i)
    {
        g_WIZNETINFO.ip[i] = ip[i];
        g_WIZNETINFO.sn[i] = netmask[i];
        g_WIZNETINFO.gw[i] = gateway[i];
        g_WIZNETINFO.dns[i] = dnsserv[i];
    }
    g_WIZNETINFO.dhcp = NETINFO_STATIC;
    wizchip_setnetinfo(&g_WIZNETINFO);

    /* check parameter set */
    wizchip_getnetinfo(&g_WIZNETINFO);
    for ( uint8_t i = 0; i <= 5; ++i )
    {
        if ( g_WIZNETINFO.mac[i] != mac[i] ) return W5500_ERR;
    }
    for ( uint8_t i = 0; i <= 3; ++i)
    {
        if ( g_WIZNETINFO.ip[i] != ip[i] ) return W5500_ERR;
        if ( g_WIZNETINFO.sn[i] != netmask[i] ) return W5500_ERR;
        if ( g_WIZNETINFO.gw[i] != gateway[i] ) return W5500_ERR;
        if ( g_WIZNETINFO.dns[i] != dnsserv[i] ) return W5500_ERR;
    }

    uint8_t phycfgr_stat;
    do
    {
        /* check  */
        if( ctlwizchip( CW_GET_PHYLINK, (void*) &phycfgr_stat ) == -1 )
        {
            /* Send info to UART console */
//            char sbuf[] = {"Error. Exit..."};
//            HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//            HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
        }
    }
    while(phycfgr_stat == PHY_LINK_OFF);

    return W5500_OK;
}


/**
 * @brief  Open TCP socket, prepare to listen
 * @retval Initialazation result - success or not
 */
uint8_t net_tcp_serv_open(uint8_t socket_number, uint8_t addr[4], uint32_t port)
{
    /* Send info to UART console */
//    {
//        char sbuf[] = {"Set IO mode"};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }

    /* Set non-block io mode */
    uint8_t io_mode = SOCK_IO_NONBLOCK;
    if ( ctlsocket(socket_number, CS_SET_IOMODE, &io_mode ) == SOCKERR_ARG )
        return W5500_ERR;

    /* Send info to UART console */
//    {
//        char sbuf[] = {"Set TCP mode and open socket"};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }
    /* Open TCP socket */
    uint8_t retval = socket( socket_number, Sn_MR_TCP, port, 0);
    if ( retval != socket_number ) return W5500_ERR;
    /* Wait for status set */
    do
    {
        /* Send info to UART console */
//        {
//            char sbuf[] = {"Sn_SR = "};
//            HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//            HAL_UART_Transmit(&huart1, (uint8_t*)getSn_SR(socket_number), 1, HAL_MAX_DELAY);
//            HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//        }
        HAL_Delay(10);
    }
    while ( getSn_SR(socket_number) != SOCK_INIT );

    retval = listen(socket_number);
    if ( retval != SOCK_OK ) return W5500_ERR;
    /* Wait for status set */
    do
    {
        /* Send info to UART console */
//        {
//            char sbuf[] = {"Sn_SR = "};
//            HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//            HAL_UART_Transmit(&huart1, (uint8_t*)getSn_SR(socket_number), 1, HAL_MAX_DELAY);
//            HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//        }
        HAL_Delay(10);
    }
    while ( getSn_SR(socket_number) != SOCK_LISTEN ) ;
    /* Send info to UART console */
//    {
//        char sbuf[] = {"Listening..."};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }
    return W5500_OK;
}


/**
 * @brief  Transmit data to client and receive data
 * @retval TX/RX result - success or not
 */
uint8_t net_tcp_serv_tx_rx(uint8_t socket_number, uint8_t *p_tx_data, uint8_t size_tx_data, uint8_t *p_rx_data)
{
    /* Wait for connection */
    while ( getSn_SR(socket_number) != SOCK_ESTABLISHED ) ;

    /* Send info to UART console */
//    {
//        char sbuf[] = {"Client accepted"};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }

    /* Get remote information */
    uint8_t remoteIP[4];
    uint16_t remotePort;
    getsockopt(socket_number, SO_DESTIP, remoteIP);
    getsockopt(socket_number, SO_DESTPORT, &remotePort);
    /* Send info to UART console */
//    {
//        char sbuf[] = {"Initialisation starts..."};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }
//    uart_print("remote IP[PORT] : %03d.%03d.%03d.%03d[%05d]\n",
//                remoteIP[0], remoteIP[1], remoteIP[2], remoteIP[3], remotePort);


    /* Transmit data */
    if ( net_tcp_serv_tx(socket_number, p_tx_data, size_tx_data) != W5500_OK )
        return W5500_ERR;

    /* Receive data */
    while (1)
    {
        if ( net_tcp_serv_rx(socket_number, p_rx_data) != W5500_OK )
            return W5500_ERR;
        /* data will be in *p_rx_data */
    }

    return W5500_OK;
}


/**
 * @brief  Receive data from W5500 and put it in buffer; before using you need to create buffer variable
 * @retval Result - success or not
 */
uint8_t net_tcp_serv_rx(uint8_t socket_number, uint8_t *buff)
{
    /* Check for data receive */
    // while ( !(getSn_IR(socket_number) & Sn_IR_RECV) ) ;
    if ( !(getSn_IR(socket_number) & Sn_IR_RECV) )
        return W5500_ERR;
    /* Clear interrupt bit */
    setSn_IR(socket_number, Sn_IR_RECV);
    /* Wait for cleaning interrupt bit */
    while ( (getSn_IR(socket_number) & Sn_IR_RECV) ) ;

    /* Read data from W5500 buffer */
    uint32_t ret = 0;
    ret = recv(socket_number, buff, 1024);
    /* Send info to UART console */
//    {
//        char sbuf[] = {"Data received"};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }

    /* Check if data is right */
    if ( ret < 0 )
    {
        /* Send info to UART console */
//        {
//            char sbuf[] = {"Error. Close socket."};
//            HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//            HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//        }
        close(socket_number);
        return W5500_ERR;
    }
    else if (ret >= 0)
    {
        buff[ret]  = 0x00;
        /* Send info to UART console */
        {
            HAL_UART_Transmit(&huart1, (uint8_t*)"Data=", 5, HAL_MAX_DELAY);
            HAL_UART_Transmit(&huart1, (uint8_t*)buff, strlen(buff), HAL_MAX_DELAY);
            HAL_UART_Transmit(&huart1, (uint8_t*)"           ", abs(11-strlen(buff)), HAL_MAX_DELAY);
            HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
        }
    }
    return W5500_OK;
}


/**
 * @brief  Transmit data to W5500
 * @retval Result - success or not
 */
uint8_t net_tcp_serv_tx(uint8_t socket_number, uint8_t *buff, uint8_t buff_size)
{
    int32_t ret = 0;
    ret = send(socket_number, buff, buff_size);
    if (ret < 0)
    {
        /* In fact it isn't right code, but it works \_(ї)_/ */

        /* Send info to UART console */
//        {
//            char sbuf1[] = {"Send failed = "};
//            HAL_UART_Transmit(&huart1, (uint8_t*)sbuf1, strlen(sbuf), HAL_MAX_DELAY);
//            HAL_UART_Transmit(&huart1, (uint8_t*)ret, 1, HAL_MAX_DELAY);
//            HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//        }
//        close(socket_number);
//        return W5500_ERR;
        return W5500_OK;
    }
    else
    {
        /* Sending success */
        return W5500_OK;
    }
}


/**
 * @brief  Close socket
 * @retval Result - success or not
 */
uint8_t net_tcp_serv_close(uint8_t socket_number)
{
    /* Close opened socket */
    if ( close(socket_number) != SOCK_OK ) return W5500_ERR;
    /* Send info to UART console */
//    {
//        char sbuf[] = {"Socket closed"};
//        HAL_UART_Transmit(&huart1, (uint8_t*)sbuf, strlen(sbuf), HAL_MAX_DELAY);
//        HAL_UART_Transmit(&huart1, (uint8_t*)"\n\r", 2, HAL_MAX_DELAY);
//    }

    return W5500_OK;
}


/**
 * @brief  Check socket status
 * @retval Socket status
 */
uint8_t net_tcp_serv_status(uint8_t socket_number)
{
    /* Check connection status */
    switch ( getSn_SR(socket_number) )
    {
    case SOCK_INIT:
    case SOCK_LISTEN:
    case SOCK_ESTABLISHED:
    case SOCK_SYNSENT:
    case SOCK_SYNRECV:
        return W5500_OK;
        break;
    }
    return W5500_ERR;
}
