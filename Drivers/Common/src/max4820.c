/*
 * max4820.c
 *
 *  Created on: Mar 13, 2021
 *  Author: Anton Shein<anton-shein2008@yandex.ru>
 *  ----------------------------------------------------------------------
 *  Copyright (C) Anton Shein, 2021
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later Bversion.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ----------------------------------------------------------------------
 */

#include "main.h"
#include "max4820.h"

extern SPI_HandleTypeDef SPIx;


uint8_t max_init(MAX_TypeDefChip *max)
{
    /* Define amount of chips */
    if (max->number < 1 || max->number > 4)
    {
        return HAL_ERROR;
    }

    /* Reset MAX state */
    HAL_GPIO_WritePin(MAX_CS_GPIO_Port, MAX_CS_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(MAX_CS_GPIO_Port, MAX_CS_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(MAX_CS_GPIO_Port, MAX_CS_Pin, GPIO_PIN_RESET);
    uint8_t null_dat = 0x00;
    for (int i = 0; i < max->number; ++i)
    {
        HAL_SPI_Transmit(&SPIx, &null_dat, 1, HAL_MAX_DELAY);
    }
    HAL_GPIO_WritePin(MAX_CS_GPIO_Port, MAX_CS_Pin, GPIO_PIN_SET);

    return HAL_OK;
}


uint8_t max_set_out(MAX_TypeDefChip *max)
{
    /* CS is low to start transmition */
    HAL_GPIO_WritePin(MAX_CS_GPIO_Port, MAX_CS_Pin, GPIO_PIN_RESET);

    /* Transmit state */
    for (int i = (max->number - 1); i >= 0; --i)
        HAL_SPI_Transmit(&SPIx, &(max->chip[i]), 1, HAL_MAX_DELAY);

    /* Set state */
    HAL_GPIO_WritePin(MAX_CS_GPIO_Port, MAX_CS_Pin, GPIO_PIN_SET);

    return HAL_OK;
}


uint8_t max_read(MAX_TypeDefChip *max)
{
    /* CS is low to start transmition */
    HAL_GPIO_WritePin(MAX_CS_GPIO_Port, MAX_CS_Pin, GPIO_PIN_RESET);


    uint8_t null_dat = 0x00;
    /* Read state */
    for (int i = (max->number - 1); i >= 0; --i)
    {
        HAL_SPI_TransmitReceive(&SPIx, &null_dat, (max->chip + i), 1, HAL_MAX_DELAY);
    }
    /* Transmit state */
    for (int i = (max->number - 1); i >= 0; --i)
    {
        HAL_SPI_Transmit(&SPIx, (max->chip + i), 1, HAL_MAX_DELAY);
    }


    /* Set state */
    HAL_GPIO_WritePin(MAX_CS_GPIO_Port, MAX_CS_Pin, GPIO_PIN_SET);

    return HAL_OK;
}
