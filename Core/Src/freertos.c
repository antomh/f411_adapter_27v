/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "string.h"
#include "net.h"
#include "usart.h"
#include "max4820.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

extern uint8_t user_socket;
extern uint8_t destip[4];
extern uint32_t destport;
extern UART_HandleTypeDef huart1;
extern MAX_TypeDefChip max;
uint8_t ethBuff[1024];

/* USER CODE END Variables */
osThreadId idleTaskHandle;
osThreadId chkEthConnHandle;
osThreadId eth_rx_txHandle;
osThreadId statHandle;
osThreadId rstHandle;
osThreadId testHandle;
osThreadId idnHandle;
osThreadId systHandle;
osThreadId outpHandle;
osThreadId currProtectHandle;
osSemaphoreId cmdIdnHandle;
osSemaphoreId cmdRstHandle;
osSemaphoreId cmdStatHandle;
osSemaphoreId cmdTestHandle;
osSemaphoreId cmdOutpHandle;
osSemaphoreId cmdSystHandle;
osSemaphoreId overcurrentProtectionHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void f_idleTask(void const * argument);
void f_chkEthConn(void const * argument);
void f_eth_rx_tx(void const * argument);
void f_stat(void const * argument);
void f_rst(void const * argument);
void f_test(void const * argument);
void f_idn(void const * argument);
void f_syst(void const * argument);
void f_outp(void const * argument);
void f_currProt(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{

}

__weak unsigned long getRunTimeCounterValue(void)
{
return 0;
}
/* USER CODE END 1 */

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of cmdIdn */
  osSemaphoreDef(cmdIdn);
  cmdIdnHandle = osSemaphoreCreate(osSemaphore(cmdIdn), 1);

  /* definition and creation of cmdRst */
  osSemaphoreDef(cmdRst);
  cmdRstHandle = osSemaphoreCreate(osSemaphore(cmdRst), 1);

  /* definition and creation of cmdStat */
  osSemaphoreDef(cmdStat);
  cmdStatHandle = osSemaphoreCreate(osSemaphore(cmdStat), 1);

  /* definition and creation of cmdTest */
  osSemaphoreDef(cmdTest);
  cmdTestHandle = osSemaphoreCreate(osSemaphore(cmdTest), 1);

  /* definition and creation of cmdOutp */
  osSemaphoreDef(cmdOutp);
  cmdOutpHandle = osSemaphoreCreate(osSemaphore(cmdOutp), 1);

  /* definition and creation of cmdSyst */
  osSemaphoreDef(cmdSyst);
  cmdSystHandle = osSemaphoreCreate(osSemaphore(cmdSyst), 1);

  /* definition and creation of overcurrentProtection */
  osSemaphoreDef(overcurrentProtection);
  overcurrentProtectionHandle = osSemaphoreCreate(osSemaphore(overcurrentProtection), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */

  /* Take the semaphores to avoid its first unexpected take */
  osSemaphoreWait(cmdIdnHandle, 1);
  osSemaphoreWait(cmdRstHandle, 1);
  osSemaphoreWait(cmdStatHandle, 1);
  osSemaphoreWait(cmdTestHandle, 1);
  osSemaphoreWait(cmdOutpHandle, 1);
  osSemaphoreWait(cmdSystHandle, 1);
  osSemaphoreWait(overcurrentProtectionHandle, 1);

  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of idleTask */
  osThreadDef(idleTask, f_idleTask, osPriorityIdle, 0, 128);
  idleTaskHandle = osThreadCreate(osThread(idleTask), NULL);

  /* definition and creation of chkEthConn */
  osThreadDef(chkEthConn, f_chkEthConn, osPriorityLow, 0, 128);
  chkEthConnHandle = osThreadCreate(osThread(chkEthConn), NULL);

  /* definition and creation of eth_rx_tx */
  osThreadDef(eth_rx_tx, f_eth_rx_tx, osPriorityIdle, 0, 128);
  eth_rx_txHandle = osThreadCreate(osThread(eth_rx_tx), NULL);

  /* definition and creation of stat */
  osThreadDef(stat, f_stat, osPriorityIdle, 0, 128);
  statHandle = osThreadCreate(osThread(stat), NULL);

  /* definition and creation of rst */
  osThreadDef(rst, f_rst, osPriorityIdle, 0, 128);
  rstHandle = osThreadCreate(osThread(rst), NULL);

  /* definition and creation of test */
  osThreadDef(test, f_test, osPriorityIdle, 0, 128);
  testHandle = osThreadCreate(osThread(test), NULL);

  /* definition and creation of idn */
  osThreadDef(idn, f_idn, osPriorityIdle, 0, 128);
  idnHandle = osThreadCreate(osThread(idn), NULL);

  /* definition and creation of syst */
  osThreadDef(syst, f_syst, osPriorityIdle, 0, 128);
  systHandle = osThreadCreate(osThread(syst), NULL);

  /* definition and creation of outp */
  osThreadDef(outp, f_outp, osPriorityIdle, 0, 128);
  outpHandle = osThreadCreate(osThread(outp), NULL);

  /* definition and creation of currProtect */
  osThreadDef(currProtect, f_currProt, osPriorityIdle, 0, 128);
  currProtectHandle = osThreadCreate(osThread(currProtect), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_f_idleTask */
/**
  * @brief  Function implementing the idleTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_f_idleTask */
void f_idleTask(void const * argument)
{
  /* USER CODE BEGIN f_idleTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END f_idleTask */
}

/* USER CODE BEGIN Header_f_chkEthConn */
/**
* @brief Function implementing the chkEthConn thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_chkEthConn */
void f_chkEthConn(void const * argument)
{
  /* USER CODE BEGIN f_chkEthConn */
  /* Infinite loop */
  for(;;)
  {
    osDelay(15000);
    if ( net_tcp_serv_status(user_socket) != W5500_OK )
    {
        HAL_UART_Transmit(&huart1, (uint8_t*)"Connection was ABR", 18, HAL_MAX_DELAY);
        do
        {
          net_init();
        }
        while ( net_tcp_serv_open(user_socket, destip, destport) != W5500_OK );
    }
    else
        HAL_UART_Transmit(&huart1, (uint8_t*)"Connection  is  OK", 18, HAL_MAX_DELAY);
  }
  /* USER CODE END f_chkEthConn */
}

/* USER CODE BEGIN Header_f_eth_rx_tx */
/**
* @brief Function implementing the eth_rx_tx thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_eth_rx_tx */
void f_eth_rx_tx(void const * argument)
{
  /* USER CODE BEGIN f_eth_rx_tx */
  /* Infinite loop */
  for(;;)
  {
    osDelay(250);
    if ( net_tcp_serv_rx(user_socket, ethBuff) != W5500_OK )
      continue;

    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);

    /* Command parsing */
    switch ( ethBuff[0] )
    {
    case 0x2A: /* 0x2A = *  Common command */
      if ( memcmp( ethBuff, "*IDN?", strlen("*IDN?")) == 0 )
          osSemaphoreRelease(cmdIdnHandle);
      else if ( memcmp( ethBuff, "*RST", strlen("*RST")) == 0 )
          osSemaphoreRelease(cmdRstHandle);
      else if ( memcmp( ethBuff, "*STB?", strlen("*STB?")) == 0 )
          osSemaphoreRelease(cmdStatHandle);
      else if ( memcmp( ethBuff, "*TST?", strlen("*TST?")) == 0 )
          osSemaphoreRelease(cmdTestHandle);
      break;

    case 0x3A: /* 0x3A = :  Subsystem command */
      if ( memcmp( ethBuff, ":OUTP", strlen(":OUTP")) == 0)
          osSemaphoreRelease(cmdOutpHandle);
      else if ( memcmp( ethBuff, ":SYST", strlen(":SYST")) == 0)
          osSemaphoreRelease(cmdSystHandle);
      break;

    default:  /* Any wrong msg */
      HAL_UART_Transmit(&huart1, (uint8_t*)"Default           ", 18, HAL_MAX_DELAY);
      break;
    }
  }
  /* USER CODE END f_eth_rx_tx */
}

/* USER CODE BEGIN Header_f_stat */
/**
* @brief Function implementing the stat thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_stat */
void f_stat(void const * argument)
{
  /* USER CODE BEGIN f_stat */
  /* Infinite loop */
  for(;;)
  {
    if ( osSemaphoreWait(cmdStatHandle, portMAX_DELAY) != osOK )
        continue;
    HAL_UART_Transmit(&huart1, (uint8_t*)"*STB? request acpt", 18, HAL_MAX_DELAY);
    net_tcp_serv_tx(user_socket, (uint8_t*)"0", 1);
  }
  /* USER CODE END f_stat */
}

/* USER CODE BEGIN Header_f_rst */
/**
* @brief Function implementing the rst thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_rst */
void f_rst(void const * argument)
{
  /* USER CODE BEGIN f_rst */
  /* Infinite loop */
  for(;;)
  {
    if ( osSemaphoreWait(cmdRstHandle, portMAX_DELAY) != osOK )
        continue;
    HAL_UART_Transmit(&huart1, (uint8_t*)"*RST  request acpt", 18, HAL_MAX_DELAY);
    /*
     * RESET CODE
     * */
  }
  /* USER CODE END f_rst */
}

/* USER CODE BEGIN Header_f_test */
/**
* @brief Function implementing the test thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_test */
void f_test(void const * argument)
{
  /* USER CODE BEGIN f_test */
  /* Infinite loop */
  for(;;)
  {
    if ( osSemaphoreWait(cmdTestHandle, portMAX_DELAY) != osOK )
        continue;
    HAL_UART_Transmit(&huart1, (uint8_t*)"*TST? request acpt", 18, HAL_MAX_DELAY);


    /* Check relay drivers (MAX4820) value */
    MAX_TypeDefChip max_tst = {
            .number = max.number
    };
    max_read(&max_tst);
    for (int i = 0; i < max.number; ++i)
        if (max.chip[i] != max_tst.chip[i])
        {
            net_tcp_serv_tx(user_socket, (uint8_t*)"TST:ERR", strlen("TST:ERR"));
            continue;
        }
    net_tcp_serv_tx(user_socket, (uint8_t*)"TST:OK", strlen("TST:OK"));
  }
  /* USER CODE END f_test */
}

/* USER CODE BEGIN Header_f_idn */
/**
* @brief Function implementing the idn thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_idn */
void f_idn(void const * argument)
{
  /* USER CODE BEGIN f_idn */
  /* Infinite loop */
  for(;;)
  {
    if ( osSemaphoreWait(cmdIdnHandle, portMAX_DELAY) != osOK )
      continue;
    net_tcp_serv_tx(user_socket, (uint8_t*)"GRNT,ADP,SN:000,SW:011", strlen("GRNT,ADP,SN:xxx,SW:xxx"));
    HAL_UART_Transmit(&huart1, (uint8_t*)"*IDN? request acpt", 18, HAL_MAX_DELAY);
  }
  /* USER CODE END f_idn */
}

/* USER CODE BEGIN Header_f_syst */
/**
* @brief Function implementing the syst thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_syst */
void f_syst(void const * argument)
{
  /* USER CODE BEGIN f_syst */
  /* Infinite loop */
  for(;;)
  {
    if ( osSemaphoreWait(cmdSystHandle, portMAX_DELAY) != osOK )
        continue;
    HAL_UART_Transmit(&huart1, (uint8_t*)":SYST request acpt", 18, HAL_MAX_DELAY);

    if ( memcmp( (ethBuff + strlen(":SYST")), ":CHAM", 5) == 0 )
    {
      uint8_t chip_amount = ethBuff[strlen(":SYST:CHAM")];
      max.number = chip_amount;
      HAL_UART_Transmit(&huart1, (uint8_t*)"Chip number =     ", abs(18-strlen(max.number)), HAL_MAX_DELAY);
      HAL_UART_Transmit(&huart1, &max.number, 1, HAL_MAX_DELAY);
    }
  }
  /* USER CODE END f_syst */
}

/* USER CODE BEGIN Header_f_outp */
/**
* @brief Function implementing the outp thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_outp */
void f_outp(void const * argument)
{
  /* USER CODE BEGIN f_outp */
  /* Infinite loop */
  for(;;)
  {
    if ( osSemaphoreWait(cmdOutpHandle, portMAX_DELAY) != osOK )
      continue;
    HAL_UART_Transmit(&huart1, (uint8_t*)":OUTP request acpt", 18, HAL_MAX_DELAY);

    if ( memcmp( (ethBuff + strlen(":OUTP")), ":SET", 4) == 0 )
    {
      for ( int i = 0; i < max.number; ++i )
          max.chip[i] = ethBuff[strlen(":OUTP:SET") + i];
      max_set_out(&max);
    }
    else if ( memcmp( (ethBuff + strlen(":OUTP")), ":GET", 4) == 0 )
    {
      MAX_TypeDefChip tmp_max = {
              .number = max.number
      };
      tmp_max.number = max.number;
      max_read(&tmp_max);
      net_tcp_serv_tx(user_socket, tmp_max.chip, tmp_max.number);
    }
    else
      HAL_UART_Transmit(&huart1, (uint8_t*)"Wrong command     ", 18, HAL_MAX_DELAY);
  }
  /* USER CODE END f_outp */
}

/* USER CODE BEGIN Header_f_currProt */
/**
* @brief Function implementing the currProtect thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_f_currProt */
void f_currProt(void const * argument)
{
  /* USER CODE BEGIN f_currProt */
  /* Infinite loop */
  for(;;)
  {
    if ( osSemaphoreWait(overcurrentProtectionHandle, portMAX_DELAY) != osOK )
        continue;
    HAL_UART_Transmit(&huart1, (uint8_t*)"EXTI occures      ", 18, HAL_MAX_DELAY);
  }
  /* USER CODE END f_currProt */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
