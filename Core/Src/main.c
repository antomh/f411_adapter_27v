/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "i2c.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "stdio.h"
#include "string.h"
#include "stdarg.h"
#include "net.h"
#include "max4820.h"
#include "mcp23017.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

uint8_t destip[4] = {192, 168, 22, 17};
uint32_t destport = 50505;
uint8_t user_socket = 0;
/* MAX variable definition */
MAX_TypeDefChip max = {
        .number = 1
};
/* MCP variable definition */
MCP_PinConfigDef mcp;
/* Semaphore definition */
extern osSemaphoreId overcurrentProtectionHandle;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_SPI1_Init();
  MX_SPI2_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);

  /*-----MAX4820 initialization-----*/
  while( max_init(&max) != HAL_OK );
  /*-----W5500 initialization-------*/
  do
  {
      net_init();
  }
  while ( net_tcp_serv_open(user_socket, destip, destport) != W5500_OK );
  /*-----MCP23017 initialization----*/
  mcp_init();
  mcp.mode                  = MCP_PIN_Mode_In;
  mcp.membank               = MCP_PIN_MemBank_Divide_Off;
  mcp.in.polar              = MCP_PIN_In_Polar_Same;
  mcp.in.pullup             = MCP_PIN_In_PullUp_Off;
  mcp.in.interrupt.state    = MCP_PIN_In_Int_State_On;
  mcp.in.interrupt.mirror   = MCP_PIN_In_Int_Mirror_On;
  mcp.in.interrupt.compare  = MCP_PIN_In_Int_Comp_Prev;
//  mcp.in.interrupt.compare  = MCP_PIN_In_Int_Comp_DefVal;
//  mcp.in.interrupt.defval   = MCP_PIN_In_Int_DefVal_Low;
  mcp.in.interrupt.mode     = MCP_PIN_In_Int_Mode_ActDriver;
  mcp.in.interrupt.pinlvl   = MCP_PIN_In_Int_PinLvl_Low;

  if ( mcp_pin_config(&mcp, MCP_PORT_A, MCP_PIN_1) == MCP_ERR )
        HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
  uint8_t tmp;
  while (1)
  {
      tmp = mcp_read_reg(MCP_INTFA);
      if (tmp == MCP_ERR) HAL_UART_Transmit(&huart1, (uint8_t*)"ERROR   ", 8, HAL_MAX_DELAY);
      HAL_UART_Transmit(&huart1, (uint8_t*)"INTFA = ", 8, HAL_MAX_DELAY);
      HAL_UART_Transmit(&huart1, &tmp, 1, HAL_MAX_DELAY);
//      tmp = mcp_read_reg(MCP_INTCAPA);
      tmp = mcp_read_reg(MCP_GPIOA);                        // ошибка в ф-ции HAL_I2C_Master_Receive(...) - после прерывания не работает
      if (tmp == MCP_ERR) HAL_UART_Transmit(&huart1, (uint8_t*)"ERROR   ", 8, HAL_MAX_DELAY);
//      HAL_UART_Transmit(&huart1, (uint8_t*)"INTCAPA=", 8, HAL_MAX_DELAY);
      HAL_UART_Transmit(&huart1, (uint8_t*)"GPIOA = ", 8, HAL_MAX_DELAY);
      HAL_UART_Transmit(&huart1, &tmp, 1, HAL_MAX_DELAY);
      HAL_Delay(4000);
  }

//  if ( mcp_pin_config(&mcp, MCP_PORT_A, MCP_PIN_0|MCP_PIN_1|
//                                        MCP_PIN_2|MCP_PIN_3|
//                                        MCP_PIN_4|MCP_PIN_5|
//                                        MCP_PIN_6|MCP_PIN_7) == MCP_ERR )
//      HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);

//  if ( mcp_pin_config(&mcp, MCP_PORT_B, MCP_PIN_0|MCP_PIN_1|
//                                        MCP_PIN_2|MCP_PIN_3|
//                                        MCP_PIN_4|MCP_PIN_5|
//                                        MCP_PIN_6|MCP_PIN_7) == MCP_ERR )
//      HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);

  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();
  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 200;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == MCP_INT_Pin)
        osSemaphoreRelease(overcurrentProtectionHandle);
}

/* USER CODE END 4 */

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM11 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM11) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
