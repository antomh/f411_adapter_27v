from tkinter import *
import socket



def connect(event):
    global flag_con;
    
    if flag_con:

        # Действия с соединением
        try:
            ## Создаем сокет и подключаемся каждый раз при нажатии "Установить соединение"
            global conn;
            conn = socket.socket();
            conn.settimeout(1);
            conn.connect(('192.168.11.155', 50505));
            global number;
            number = sp_box.get();
            cmd = bytearray( ":SYST:CHAM", "ASCII" );
            cmd.append(int(number));
            print("Установка количества драйверов реле:", cmd);
            conn.send(cmd);
        except socket.error:
            lbl_con.config(text="Не удалось соединиться");
            return ;
            
        # Действия с интерфейсом
        if number == "1":
            for i in range(8):
                chk_1[i].config(state=NORMAL);
        elif number == "2":
            for i in range(8):
                chk_1[i].config(state=NORMAL);
                chk_2[i].config(state=NORMAL);
        elif number == "3":
            for i in range(8):
                chk_1[i].config(state=NORMAL);
                chk_2[i].config(state=NORMAL);
                chk_3[i].config(state=NORMAL);
        elif number == "4":
            for i in range(8):
                chk_1[i].config(state=NORMAL);
                chk_2[i].config(state=NORMAL);
                chk_3[i].config(state=NORMAL);
                chk_4[i].config(state=NORMAL);
        else:
            lbl_con.config(text="Установлено некорректное количество микросхем");
            for i in range(8):
                chk_1[i].config(state=DISABLED);
                chk_2[i].config(state=DISABLED);
                chk_3[i].config(state=DISABLED);
                chk_4[i].config(state=DISABLED);
        btn_con.config(text="Разорвать соединение");
        lbl_con.config(text="Соединение установлено");
        canv.create_oval(3, 3, 14, 14, fill='green');
        btn_set.config(state=NORMAL);
        btn_state.config(state=NORMAL);
        btn_send.config(state=NORMAL);
        btn_idn.config(state=NORMAL);
        btn_rst.config(state=NORMAL);
        btn_tst.config(state=NORMAL);
        btn_stb.config(state=NORMAL);
        txt_cmd.config(state=NORMAL);
        txt_answ.config(state=NORMAL);
        flag_con = 0;
    else:
        
        try:
            conn.close();
        except socket.error:
            lbl_con.config(text="Не удалось отсоединиться");
            return ;
        
        for i in range(8):
            chk_1[i].config(state=DISABLED);
            chk_2[i].config(state=DISABLED);
            chk_3[i].config(state=DISABLED);
            chk_4[i].config(state=DISABLED);
        btn_con.config(text="Установить соединение");
        lbl_con.config(text="Нет соединения");
        canv.create_oval(3, 3, 14, 14, fill='red');
        btn_set.config(state=DISABLED);
        btn_state.config(state=DISABLED);
        btn_send.config(state=DISABLED);
        btn_idn.config(state=DISABLED);
        btn_rst.config(state=DISABLED);
        btn_tst.config(state=DISABLED);
        btn_stb.config(state=DISABLED);
        txt_cmd.config(state=DISABLED);
        txt_answ.config(state=DISABLED);
        flag_con = 1;



def set_state(event):
    print("Посылка команды установки значений");
    endcmd_ind = len(":OUTP:SET")
    msg = bytearray( ":OUTP:SET", "ASCII" );
    msg.extend( [0] * (int(number)) );
    
    if number == "1":
        for i in range(8):
            msg[endcmd_ind+0] += dr_1[7-i].get() << i;
    elif number == "2":
        for i in range(8):
            msg[endcmd_ind+0] += dr_1[7-i].get() << i;
            msg[endcmd_ind+1] += dr_2[7-i].get() << i;
    elif number == "3":
        for i in range(8):
            msg[endcmd_ind+0] += dr_1[7-i].get() << i;
            msg[endcmd_ind+1] += dr_2[7-i].get() << i;
            msg[endcmd_ind+2] += dr_3[7-i].get() << i;
    elif number == "4":
        for i in range(8):
            msg[endcmd_ind+0] += dr_1[7-i].get() << i;
            msg[endcmd_ind+1] += dr_2[7-i].get() << i;
            msg[endcmd_ind+2] += dr_3[7-i].get() << i;
            msg[endcmd_ind+3] += dr_4[7-i].get() << i;

    print("Посланная команда: ", msg);
    try:
        conn.send(msg);
    except:
        pass;



def get_state(event):
    print("Посылка команды получения состояния");
    conn.send(bytearray( ":OUTP:GET", "ASCII" ));
    try:
        rec_data = conn.recv(32);
    except socket.error:
        lbl_con.config(text="Ошибка команды получения состояния");
    except socket.timeout:
        lbl_con.config(text="Таймаут команды получения состояния");
    except:
        lbl_con.config(text="Some error");
    rec_data = int.from_bytes(rec_data, "big");
    print(hex(rec_data));
    if number == "1":
        for i in range(8):
            dr_1[i].set( ( rec_data >> (7-i) ) & 0x01 );
    elif number == "2":
        for i in range(8):
            dr_1[i].set( ( rec_data >> (7-i) ) & 0x01 );
            dr_2[i].set( ( rec_data >> (15-i) ) & 0x01 );
    elif number == "3":
        for i in range(8):
            dr_1[i].set( ( rec_data >> (7-i) ) & 0x01 );
            dr_2[i].set( ( rec_data >> (15-i) ) & 0x01 );
            dr_3[i].set( ( rec_data >> (23-i) ) & 0x01 );
    elif number == "4":
        for i in range(8):
            dr_1[i].set( ( rec_data >> (7-i) ) & 0x01 );
            dr_2[i].set( ( rec_data >> (15-i) ) & 0x01 );
            dr_3[i].set( ( rec_data >> (23-i) ) & 0x01 );
            dr_4[i].set( ( rec_data >> (31-i) ) & 0x01 );



def cmd_send (event):
    cmd = bytearray( txt_cmd.get(), "ASCII" );
    try:
        conn.send(cmd);
        lbl_con.config(text="Посылка послана");
    except socket.error:
        lbl_con.config(text="Ошибка отправки команды");
    except socket.timeout:
        lbl_con.config(text="Таймаут отправки команды");
    except:
        lbl_con.config(text="Some error");



def cmd_idn (event):
    cmd = bytearray( "*IDN?", "ASCII" );
    try:
        conn.send(cmd);
        lbl_con.config(text="Команда *IDN? послана");
    except socket.error:
        lbl_con.config(text="Ошибка отправки команды");
    except socket.timeout:
        lbl_con.config(text="Таймаут отправки команды");
    except:
        lbl_con.config(text="Some error");
    print("Начало приема ответа на запрос *IDN?");
    try:
        rec_data = conn.recv(32);
        lbl_con.config(text="Ответ *IDN? получен");
        txt_answ.delete(0, END);
        txt_answ.insert(0, rec_data);
        return;
    except socket.error:
        lbl_con.config(text="Ошибка запроса *IDN?");
    except socket.timeout:
        lbl_con.config(text="Таймаут запроса *IDN?");
    except:
        lbl_con.config(text="Some error");
    txt_answ.delete(0, END);
    txt_answ.insert(0, "<ERROR>");



def cmd_tst (event):
    cmd = bytearray( "*TST?", "ASCII" );
    try:
        conn.send(cmd);
        lbl_con.config(text="Команда *TST? послана");
    except socket.error:
        lbl_con.config(text="Ошибка отправки команды");
    except socket.timeout:
        lbl_con.config(text="Таймаут отправки команды");
    except:
        lbl_con.config(text="Some error");
    print("Начало приема ответа на запрос *TST?");
    try:
        rec_data = conn.recv(32);
        lbl_con.config(text="Ответ *TST? получен");
        txt_answ.delete(0, END);
        txt_answ.insert(0, rec_data);
        return;
    except socket.error:
        lbl_con.config(text="Ошибка запроса *TST?");
    except socket.timeout:
        lbl_con.config(text="Таймаут запроса *TST?");
    except:
        lbl_con.config(text="Some error");
    txt_answ.delete(0, END);
    txt_answ.insert(0, "<ERROR>");



def cmd_rst (event):
    cmd = bytearray( "*RST", "ASCII" );
    try:
        conn.send(cmd);
        lbl_con.config(text="Команда *RST послана");
        txt_answ.delete(0, END);
        return;
    except socket.error:
        lbl_con.config(text="Ошибка отправки команды");
    except socket.timeout:
        lbl_con.config(text="Таймаут отправки команды");
    except:
        lbl_con.config(text="Some error");
    txt_answ.delete(0, END);
    txt_answ.insert(0, "<ERROR>");



def cmd_stb (event):
    cmd = bytearray( "*STB?", "ASCII" );
    try:
        conn.send(cmd);
        lbl_con.config(text="Команда *STB? послана");
    except socket.error:
        lbl_con.config(text="Ошибка отправки команды");
    except socket.timeout:
        lbl_con.config(text="Таймаут отправки команды");
    except:
        lbl_con.config(text="Some error");
    print("Начало приема ответа на запрос *STB?");
    try:
        rec_data = conn.recv(32);
        lbl_con.config(text="Ответ *STB? получен");
        txt_answ.delete(0, END);
        txt_answ.insert(0, rec_data);
        return;
    except socket.error:
        lbl_con.config(text="Ошибка запроса *STB?");
    except socket.timeout:
        lbl_con.config(text="Таймаут запроса *STB?");
    except:
        lbl_con.config(text="Some error");
    txt_answ.delete(0, END);
    txt_answ.insert(0, "<ERROR>");
        


##########Взаимодействите по TCP##########


##########Графический интерфейс###########

# Создание некоторых полезных далее переменных
flag_con = 1;
global rec_data;

# Создание окна
window = Tk();
window.title("Адаптер 27В");
window.resizable(False, False);

# Задание областей окна
lbl_1 = LabelFrame(window,
                   text="Состояние соединения",
                   font="Arial 10 bold");
lbl_1.grid(row=0,
           column=0,
           sticky=W+E,
           padx=10,
           pady=5);

lbl_2 = LabelFrame(window,
                   text="Управление реле",
                   font="Arial 10 bold");
lbl_2.grid(row=1,
           column=0,
           sticky=W+E,
           padx=10,
           pady=5);

lbl_3 = LabelFrame(window,
                   text="Посылка предустановленных SCPI команд",
                   font="Arial 10 bold");
lbl_3.grid(row=2,
           column=0,
           sticky=W+E,
           padx=10,
           pady=5);

lbl_4 = LabelFrame(window,
                   text="Посылка команд вручную",
                   font="Arial 10 bold");
lbl_4.grid(row=3,
           column=0,
           sticky=W+E,
           padx=10,
           pady=5);

# Элементы внутри первой области
## Текст статуса
lbl_con = Label(lbl_1,
                text="Нет соединения",
                font="Arial 10 italic");
lbl_con.grid(row=0,
             column=0,
             columnspan=2,
             sticky=W,
             padx=4,
             pady=4);

## Кружок статуса
canv = Canvas(lbl_1,
              width=14,
              height=14);
canv.grid(row=0,
          column=2);
canv.create_oval(3, 3, 14, 14,
                 fill='red');

Label(lbl_1, text="Количество драйверов: ") \
             .grid(row=1,
                   column=0,
                   sticky=W,
                   padx=4,
                   pady=4);

## Ввод количества микросхем драйвера
sp_box = Spinbox(lbl_1,
                 width=18,
                 from_=1,
                 to=4,
                 state='readonly');
sp_box.grid(row=1,
            column=1,
            padx=4,
            pady=4);

## Кнопка установки соединения
btn_con = Button(lbl_1,
                 text="Установить соединение",
                 width=19);
btn_con.grid(row=1,
             column=2,
             sticky=E,
             padx=4,
             pady=4);
btn_con.bind('<Button-1>', connect);

# Элементы внутри второй области
Label(lbl_2, text="Пины драйверов")\
             .grid(row=0,
                   column=1,
                   columnspan=9,
                   padx=4,
                   pady=4);
Label(lbl_2, text="Первый драйвер")\
             .grid(row=1,
                   column=0,
                   sticky=W,
                   padx=4,
                   pady=4);
Label(lbl_2, text="Второй драйвер")\
             .grid(row=2,
                   column=0,
                   sticky=W,
                   padx=4,
                   pady=4);
Label(lbl_2, text="Третий драйвер")\
             .grid(row=3,
                   column=0,
                   sticky=W,
                   padx=4,
                   pady=4);
Label(lbl_2, text="Червертый драйвер")\
             .grid(row=4,
                   column=0,
                   sticky=W,
                   padx=4,
                   pady=4);

## Checkbox'ы
dr_1 = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()];
chk_1 = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()];
for i in range(8):
    dr_1[i].set(0);
    chk_1[i] = Checkbutton(lbl_2,
                           text=8-i,
                           variable=dr_1[i],
                           onvalue=1,
                           offvalue=0,
                           state=DISABLED);
    chk_1[i].grid(row=1,
                  column=i+1,
                  padx=2,
                  pady=2);
    
dr_2 = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()];
chk_2 = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()];
for i in range(8):
    dr_2[i].set(0);
    chk_2[i] = Checkbutton(lbl_2,
                           text=8-i,
                           variable=dr_2[i],
                           onvalue=1,
                           offvalue=0,
                           state=DISABLED);
    chk_2[i].grid(row=2,
                  column=i+1,
                  padx=2,
                  pady=2);

dr_3 = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()];
chk_3 = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()];
for i in range(8):
    dr_3[i].set(0);
    chk_3[i] = Checkbutton(lbl_2,
                           text=8-i,
                           variable=dr_3[i],
                           onvalue=1,
                           offvalue=0,
                           state=DISABLED);
    chk_3[i].grid(row=3,
                  column=i+1,
                  padx=2,
                  pady=2);
    
dr_4 = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()];
chk_4 = [IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()];
for i in range(8):
    dr_4[i].set(0);
    chk_4[i] = Checkbutton(lbl_2,
                           text=8-i,
                           variable=dr_4[i],
                           onvalue=1,
                           offvalue=0,
                           state=DISABLED);
    chk_4[i].grid(row=4,
                  column=i+1,
                  padx=2,
                  pady=2);

## Кнопки команд
btn_set = Button(lbl_2,
                 text="Установить выводы",
                 state=DISABLED);
btn_set.grid(row=6,
             column=0,
             columnspan=9,
             sticky=W+E,
             padx=4,
             pady=4);
btn_set.bind('<Button-1>', set_state);

btn_state = Button(lbl_2,
                   text="Получить состояние выводов",
                   state=DISABLED);
btn_state.grid(row=7,
               column=0,
               columnspan=9,
               sticky=W+E,
               padx=4,
               pady=4);
btn_state.bind('<Button-1>', get_state);

# Элементы внутри третей области

btn_idn = Button(lbl_3,
                 text="*IDN?",
                 state=DISABLED);
btn_idn.grid(row=0,
             column=0,
             sticky=W+E,
             padx=4,
             pady=4);
btn_idn.bind('<Button-1>', cmd_idn);

btn_tst = Button(lbl_3,
                 text="*TST?",
                 state=DISABLED);
btn_tst.grid(row=0,
             column=1,
             sticky=W+E,
             padx=4,
             pady=4);
btn_tst.bind('<Button-1>', cmd_tst);

btn_rst = Button(lbl_3,
                 text="*RST",
                 state=DISABLED);
btn_rst.grid(row=0,
             column=2,
             sticky=W+E,
             padx=4,
             pady=4);
btn_rst.bind('<Button-1>', cmd_rst);

btn_stb = Button(lbl_3,
                 text="*STB?",
                 state=DISABLED);
btn_stb.grid(row=0,
             column=3,
             sticky=W+E,
             padx=4,
             pady=4);
btn_stb.bind('<Button-1>', cmd_stb);

Label(lbl_3, text="Ответ:", font="Courier 10 bold")\
             .grid(row=1,
                   column=0,
                   columnspan=4,
                   sticky=W,
                   padx=4,
                   pady=4);

txt_answ = Entry(lbl_3,
                 width=69,
                 state=DISABLED);
txt_answ.grid(row=2,
              column=0,
              columnspan=4,
              sticky=W+E,
              padx=4,
              pady=4);

# Элементы внутри четвертой области

txt_cmd = Entry(lbl_4,
                width=56,
                state=DISABLED);
txt_cmd.grid(row=0,
             column=0,
             sticky=W,
             padx=4,
             pady=4);

btn_send = Button(lbl_4,
                 text="Отправить",
                 state=DISABLED);
btn_send.grid(row=0,
             column=1,
             sticky=E,
             padx=4,
             pady=4);
btn_send.bind('<Button-1>', cmd_send);

# Начало бесконечного цикла
window.mainloop();
